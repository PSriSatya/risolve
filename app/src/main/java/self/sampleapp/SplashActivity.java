package self.sampleapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        findViewById(R.id.tvIconText).postDelayed(new Runnable() {
            @Override
            public void run() {
                resolveNextPage();
                finish();
            }
        }, 3000);
    }

    // https://github.com/apl-devs/AppIntro/wiki/How-to-Use#show-the-intro-once
    private void resolveNextPage() {
        SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean isFirstStart = getPrefs.getBoolean("firstStart", true);
        isFirstStart = true;
        //  If the activity has never started before...
        if (isFirstStart) {
            Intent i = new Intent(SplashActivity.this, LaunchActivity.class);
            startActivity(i);
            SharedPreferences.Editor e = getPrefs.edit();
            e.putBoolean("firstStart", false);
            e.apply();
        } else {
            Intent i = new Intent(SplashActivity.this, DashboardActivity.class);
            startActivity(i);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}