package self.sampleapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import self.sampleapp.R;
import self.sampleapp.model.CommunityUpdateDO;

/**
 * Created by srikrishna on 08-07-2017.
 */

public class CommunityUpdatesAdapter extends RecyclerView.Adapter<CommunityUpdatesAdapter.ViewHolder> {

    private static final String TAG = "CommunityUpdatesAdapter";

    private ArrayList<CommunityUpdateDO> mDataSet;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle, tvDesc, tvWhen, tvWhere;
        public ImageView ivImage;

        public ViewHolder(View v) {
            super(v);
            ivImage = (ImageView) v.findViewById(R.id.ivImage);
            tvTitle = (TextView) v.findViewById(R.id.tvTitle);
            tvDesc = (TextView) v.findViewById(R.id.tvDesc);
            tvWhen = (TextView) v.findViewById(R.id.tvWhen);
            tvWhere = (TextView) v.findViewById(R.id.tvWhere);
        }
    }

    public CommunityUpdatesAdapter(ArrayList<CommunityUpdateDO> dataSet) {
        mDataSet = dataSet;
    }

    @Override
    public CommunityUpdatesAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.community_update_item, viewGroup, false);

        return new CommunityUpdatesAdapter.ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(CommunityUpdatesAdapter.ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
        CommunityUpdateDO cDO = mDataSet.get(position);
        viewHolder.tvTitle.setText(cDO.getTitle());
        viewHolder.tvDesc.setText(cDO.getDescription());
        viewHolder.tvWhen.setText(cDO.getWhen());
        viewHolder.tvWhere.setText(cDO.getWhere());
        Picasso.with(viewHolder.ivImage.getContext()).load(cDO.getImgRes()).into(viewHolder.ivImage);
    }

    @Override
    public int getItemCount() {
        if (mDataSet != null && mDataSet.size() > 0)
            return mDataSet.size();
        else return 0;
    }
}
