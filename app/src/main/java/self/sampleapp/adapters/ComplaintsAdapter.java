package self.sampleapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import self.sampleapp.R;
import self.sampleapp.model.ComplaintDO;

/**
 * Created by srikrishna on 04-07-2017.
 */

public class ComplaintsAdapter extends RecyclerView.Adapter<ComplaintsAdapter.ViewHolder> {
    private static final String TAG = "ExclusiveOffersAdapter";

    private ArrayList<ComplaintDO> mDataSet;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvComplaintStatus, tvDate, tvTime, tvComplaintNumber, tvComplaintSubject, tvCompaintDepartment;
        public ImageView ivImage;

        public ViewHolder(View v) {
            super(v);
            ivImage = (ImageView) v.findViewById(R.id.ivImage);
            tvComplaintStatus = (TextView) v.findViewById(R.id.tvComplaintStatus);
            tvDate = (TextView) v.findViewById(R.id.tvDate);
            tvTime = (TextView) v.findViewById(R.id.tvTime);
            tvComplaintNumber = (TextView) v.findViewById(R.id.tvComplaintNumber);
            tvComplaintSubject = (TextView) v.findViewById(R.id.tvComplaintSubject);
            tvCompaintDepartment = (TextView) v.findViewById(R.id.tvCompaintDepartment);
        }
    }

    public ComplaintsAdapter(ArrayList<ComplaintDO> dataSet) {
        mDataSet = dataSet;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.complaint_item, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");

        ComplaintDO eDO = mDataSet.get(position);
        viewHolder.tvComplaintStatus.setText(eDO.getComplaintStatus());
        viewHolder.tvDate.setText(eDO.getDate());
        viewHolder.tvTime.setText(eDO.getTime());
        viewHolder.tvComplaintNumber.setText(eDO.getComplaintNumber());
        viewHolder.tvComplaintSubject.setText(eDO.getComplaintSubject());
        viewHolder.tvCompaintDepartment.setText(eDO.getCompaintDepartment());
        Picasso.with(viewHolder.ivImage.getContext()).load(eDO.getImgRes()).into(viewHolder.ivImage);

    }

    @Override
    public int getItemCount() {
        if (mDataSet != null && mDataSet.size() > 0)
            return mDataSet.size();
        else return 0;
    }
}
