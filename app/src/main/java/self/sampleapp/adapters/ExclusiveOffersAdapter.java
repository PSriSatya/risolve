package self.sampleapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import self.sampleapp.R;
import self.sampleapp.model.ExclusiveOfferDO;

/**
 * Created by srikrishna on 04-07-2017.
 */

public class ExclusiveOffersAdapter extends RecyclerView.Adapter<ExclusiveOffersAdapter.ViewHolder> {
    private static final String TAG = "ExclusiveOffersAdapter";

    private ArrayList<ExclusiveOfferDO> mDataSet;

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvExOfferTitle, tvExOfferDesc, tvExOfferSub;
        public ImageView ivExOfferImage;

        public ViewHolder(View v) {
            super(v);
            ivExOfferImage = (ImageView) v.findViewById(R.id.ivExOfferImage);
            tvExOfferTitle = (TextView) v.findViewById(R.id.tvExOfferTitle);
            tvExOfferDesc = (TextView) v.findViewById(R.id.tvExOfferDesc);
            tvExOfferSub = (TextView) v.findViewById(R.id.tvExOfferSub);
        }
    }
    // END_INCLUDE(recyclerViewSampleViewHolder)

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    public ExclusiveOffersAdapter(ArrayList<ExclusiveOfferDO> dataSet) {
        mDataSet = dataSet;
    }

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.exclusive_offers_item, viewGroup, false);

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
        ExclusiveOfferDO eDO = mDataSet.get(position);
        viewHolder.tvExOfferTitle.setText(eDO.getTitle());
        viewHolder.tvExOfferDesc.setText(eDO.getDescription());
        viewHolder.tvExOfferSub.setText(eDO.getSubTitle());
        Picasso.with(viewHolder.ivExOfferImage.getContext()).load(eDO.getImgRes()).into(viewHolder.ivExOfferImage);

    }
    // END_INCLUDE(recyclerViewOnBindViewHolder)

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if (mDataSet != null && mDataSet.size() > 0)
            return mDataSet.size();
        else return 0;
    }
}
