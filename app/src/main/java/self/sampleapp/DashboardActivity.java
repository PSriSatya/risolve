package self.sampleapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


import java.util.ArrayList;

import self.sampleapp.adapters.CommunityUpdatesAdapter;
import self.sampleapp.adapters.ExclusiveOffersAdapter;
import self.sampleapp.model.CommunityUpdateDO;
import self.sampleapp.model.ExclusiveOfferDO;

public class DashboardActivity extends AppCompatActivity {


    protected RecyclerView rvExOffers, rvCommunityUpdates;
    protected RecyclerView.LayoutManager lmExOffers, lmCommunityUpdates;
    protected ExclusiveOffersAdapter aExOffers;
    protected CommunityUpdatesAdapter aCommunityUpdates;
    protected ArrayList<ExclusiveOfferDO> doExOffers;
    protected ArrayList<CommunityUpdateDO> doCommunityUpdates;
    protected TextView tvNoExOf, tvNoCoUp, tvCompaints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        initDataSet();

        tvNoExOf = (TextView) findViewById(R.id.tvNoExOf);
//        tvNoCoUp = (TextView) findViewById(R.id.tvNoCoUp);
        tvCompaints = (TextView) findViewById(R.id.tvCompaints1);

        rvExOffers = (RecyclerView) findViewById(R.id.rvExOffers);
        lmExOffers = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvExOffers.setLayoutManager(lmExOffers);
        aExOffers = new ExclusiveOffersAdapter(doExOffers);
        rvExOffers.setAdapter(aExOffers);

        rvCommunityUpdates = (RecyclerView) findViewById(R.id.rvCommunityUpdates);
        lmCommunityUpdates = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvCommunityUpdates.setLayoutManager(lmCommunityUpdates);
        aCommunityUpdates = new CommunityUpdatesAdapter(doCommunityUpdates);
        rvCommunityUpdates.setAdapter(aCommunityUpdates);
//        rvCommunityUpdates.setNestedScrollingEnabled(false);

        tvCompaints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashboardActivity.this, ComplaintsActivity.class));
            }
        });
    }

    private void initDataSet(){
        doExOffers = new ArrayList<>();
        doExOffers.add(
                new ExclusiveOfferDO(R.drawable.e_1173_f_0,
                "Rent your Property",
                "Reach out to thousands of home seekers and get genuine leads.",
                "Starts from: 20k")
        );
        doExOffers.add(
                new ExclusiveOfferDO(R.drawable.e_1173_f_1,
                        "Aryamitra Bay Hills",
                        "Aryamitra is a real estate and urban infrastructure development company.",
                        "2,3 BHK Apartment")
        );
        doExOffers.add(
                new ExclusiveOfferDO(R.drawable.e_1173_f_2,
                        "Ongoing Projects",
                        "Reach out to thousands of home seekers and get genuine leads.",
                        "3BHK Starts from: 20k")
        );

        doCommunityUpdates = new ArrayList<>();
        doCommunityUpdates.add(new CommunityUpdateDO(R.drawable.e_1173_f_3,
                "Zumba Classes:",
                "Zumba sponsered by GHF & Mayors Council.",
                "When: Wednesday @ 6:30 PM to 7:30 PM",
                "Where: Commons Room"
        ));
        doCommunityUpdates.add(new CommunityUpdateDO(R.drawable.e_1173_f_4,
                "Yoga Classes:",
                "Yoga: Sponsored by GFH &amp; Mayors’ Council.",
                "When: Every Tuesday @ 6:30 PM to 7:30 PM",
                "Where: Recreation Room"
        ));
        doCommunityUpdates.add(new CommunityUpdateDO(R.drawable.e_1173_f_5,
                "Important Announcement:",
                "Tanglewood Pool Closed due to flooding.",
                "When: This weekend",
                "Where: Common Meeting Area"
        ));
    }
}
