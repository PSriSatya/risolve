package self.sampleapp.model;

import java.io.Serializable;

/**
 * Created by srikrishna on 04-07-2017.
 */

public class ComplaintDO implements Serializable {

    public ComplaintDO(int imgRes, String complaintStatus, String Date, String Time,
                       String complaintNumber, String complaintSubject, String compaintDepartment){
        this.imgRes = imgRes;
        this.complaintStatus = complaintStatus;
        this.Date = Date;
        this.Time = Time;
        this.complaintNumber = complaintNumber;
        this.complaintSubject = complaintSubject;
        this.compaintDepartment = compaintDepartment;
    }

    private int imgRes;
    private String complaintStatus;
    private String Date;
    private String Time;
    private String complaintNumber;
    private String complaintSubject;
    private String compaintDepartment;

    public int getImgRes() {
        return imgRes;
    }

    public void setImgRes(int imgRes) {
        this.imgRes = imgRes;
    }

    public String getComplaintStatus() {
        return complaintStatus;
    }

    public void setComplaintStatus(String complaintStatus) {
        this.complaintStatus = complaintStatus;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getComplaintNumber() {
        return complaintNumber;
    }

    public void setComplaintNumber(String complaintNumber) {
        this.complaintNumber = complaintNumber;
    }

    public String getComplaintSubject() {
        return complaintSubject;
    }

    public void setComplaintSubject(String complaintSubject) {
        this.complaintSubject = complaintSubject;
    }

    public String getCompaintDepartment() {
        return compaintDepartment;
    }

    public void setCompaintDepartment(String compaintDepartment) {
        this.compaintDepartment = compaintDepartment;
    }
}
