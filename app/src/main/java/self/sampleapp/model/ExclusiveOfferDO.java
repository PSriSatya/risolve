package self.sampleapp.model;

import java.io.Serializable;

/**
 * Created by srikrishna on 04-07-2017.
 */

public class ExclusiveOfferDO implements Serializable {

    public ExclusiveOfferDO(int imgRes, String title, String description, String subTitle){
        this.imgRes = imgRes;
        this.title = title;
        this.description = description;
        this.subTitle = subTitle;
    }

    private int imgRes;
    private String title;
    private String description;
    private String subTitle;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public int getImgRes() {

        return imgRes;
    }

    public void setImgRes(int imgRes) {
        this.imgRes = imgRes;
    }
}
