package self.sampleapp;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import self.sampleapp.adapters.PromotionsPagerAdapter;
import self.sampleapp.widget.AutoScrollViewPager;

public class LaunchActivity extends AppCompatActivity {

    private AutoScrollViewPager autoScrollPager;
    private PageIndicator mIndicator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        autoScrollPager = (AutoScrollViewPager) findViewById(R.id.autoScrollPager);
        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        PromotionsPagerAdapter promotionsPagerAdapter = new PromotionsPagerAdapter(LaunchActivity.this);
        autoScrollPager.setAdapter(promotionsPagerAdapter);
//        autoScrollPager.startAutoScrollPager(autoScrollPager);
        mIndicator.setViewPager(autoScrollPager);
        autoScrollPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                switch (position)
                {
                    case 0:
//                        tvTitle.setText("Select and view");
//                        tvpagedescription.setText("a Doctor's Profile");
                        break;
                    case 1:
//                        tvTitle.setText("Choose Doctor by");
//                        tvpagedescription.setText(R.string.exp);
                        break;
                    case 2:
//                        tvTitle.setText("Find and book");
//                        tvpagedescription.setText("appointment instantly");
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(LaunchActivity.this, AuthenticateActivity.class);
                startActivity(in);
            }
        });

        findViewById(R.id.signUp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(LaunchActivity.this, DashboardActivity.class);
                startActivity(in);
            }
        });
    }
}
