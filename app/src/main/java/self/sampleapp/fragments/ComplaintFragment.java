package self.sampleapp.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;

import self.sampleapp.R;
import self.sampleapp.adapters.ComplaintsAdapter;
import self.sampleapp.model.ComplaintDO;

/**
 * Created by srikrishna on 08-07-2017.
 */

public class ComplaintFragment extends Fragment {

    protected RecyclerView rvComplaints;
    protected RecyclerView.LayoutManager lmComplanits;
    protected ComplaintsAdapter aComplanits;
    protected ArrayList<ComplaintDO> doComplanits;


    public static ComplaintFragment newInstance() {
        ComplaintFragment fragment = new ComplaintFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.complaint_fragment, container, false);

        initDataSet();

        rvComplaints = (RecyclerView) v.findViewById(R.id.rvComplaints);
        lmComplanits = new LinearLayoutManager(getActivity());
        rvComplaints.setLayoutManager(lmComplanits);
        aComplanits = new ComplaintsAdapter(doComplanits);
        rvComplaints.setAdapter(aComplanits);

        FloatingActionButton myFab = (FloatingActionButton) v.findViewById(R.id.fab_add);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Snackbar.make(v, "Complaint Add, coming soon.", Snackbar.LENGTH_SHORT).show();
            }
        });

        return v;
    }

    private void initDataSet() {
        doComplanits = new ArrayList<>();
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
        doComplanits.add(
                new ComplaintDO(R.drawable.eletricity,
                        "New",
                        "July 02, 2017",
                        "Time: 10:30 AM",
                        "Number : 982142",
                        "Subject : Washbasin Leakage",
                        "Department : Plumbing")
        );
    }
}
