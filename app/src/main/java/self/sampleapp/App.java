package self.sampleapp;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by srikrishna on 02-07-2017.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // https://github.com/chrisjenx/Calligraphy
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

}
